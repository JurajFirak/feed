package Project;

import java.util.HashMap;

/**
 *
 * @author Juraj Firák <juraj.firak@gmail.com>
 */
public interface FeedParser 
{
    HashMap getNodesMap();
    
    boolean checkPrice(double price);
}
