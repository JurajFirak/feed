package Project;

/**
 *
 * @author Juraj Firák <juraj.firak@gmail.com>
 */
public class Product 
{
    private String name;
    private String description;
    private String category;
    private String manufacturer;
    private double price;
    public static int count = 0;
    
    public Product()
    {}

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public String getCategory() {
	return category;
    }

    public void setCategory(String category) {
	this.category = category;
    }

    public String getManufacturer() {
	return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
	this.manufacturer = manufacturer;
    }

    public double getPrice() {
	return price;
    }

    public void setPrice(double price) {
	this.price = price;
    }
    
    public boolean save()
    {
	// TODO test if every value is set
	// save product to database, webservice etc.
	
	this.count++;
	
	return true;
    }
}
