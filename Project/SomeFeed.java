package Project;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Juraj Firák <juraj.firak@gmail.com>
 */
public class SomeFeed implements FeedParser 
{
    /**
     * Path to XML file
     */
    private final String xmlPath = "src\\files\\feed.xml";
    
    /**
     * Loaded XML file
     */
    private File file;
    
    /**
     * Document from XML
     */
    private Document doc;
    
    /**
     * Map names and tags for this XML structure
     */
    private HashMap nodesMap;
    
    /**
     * Minimal acceptable product price
     */
    private final double minPrice = 10.0;
    
    
    public SomeFeed() 
    {
	this.file = new File(xmlPath);
	
	this.setDocument(file);
	if (null == doc) {
	    throw new RuntimeException("XML sa nepodarilo nacitat.");
	}
	
	this.nodesMap = this.getNodesMap();
	
	NodeList shopItems = doc.getElementsByTagName("SHOPITEM");
	for (int i = 0; i < shopItems.getLength(); i++) {
	    Node shopItemNode = shopItems.item(i);
	    
	    Product product = new Product();
	    
	    String name = this.getValue(shopItemNode, this.nodesMap.get("name").toString());
	    product.setName(name);
	    
	    String description = this.getValue(shopItemNode, this.nodesMap.get("description").toString());
	    product.setDescription(description);
	    
	    String priceText = this.getValue(shopItemNode, this.nodesMap.get("price").toString());
	    double price = Double.parseDouble(priceText);
	    boolean priceOk = this.checkPrice(price);
	    if (!priceOk) {
		// log info about product with wrong price
		continue;
	    }
	    product.setPrice(price);
	    
	    String category = this.getValue(shopItemNode, this.nodesMap.get("category").toString());
	    product.setCategory(category);
	    
	    String manufacturer = this.getValue(shopItemNode, this.nodesMap.get("manufacturer").toString());
	    product.setManufacturer(manufacturer);
	    
	    product.save();
	}
	
	System.out.println("Uložených produktov: " + Product.count + ".");
    }
    
    /**
     *
     * @return
     */
    @Override
    public HashMap getNodesMap()
    {
	nodesMap = new HashMap();
	nodesMap.put("id", "ITEM_ID");
	nodesMap.put("name", "PRODUCT");
	nodesMap.put("description", "DESCRIPTION");
	nodesMap.put("image", "IMGURL");
	nodesMap.put("price", "PRICE");
	nodesMap.put("manufacturer", "MANUFACTURER");
	nodesMap.put("category", "CATEGORYTEXT");
	
	return nodesMap;
    }
    
    
    /**
     * Check price of product
     * @param price
     * @return
     */
    @Override
    public boolean checkPrice(double price)
    {
	return price >= minPrice;
    }
    
    
    /**
     * Set document from file
     * @param file 
     */
    protected void setDocument(File file) 
    {
	try {
	    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	    DocumentBuilder db = dbf.newDocumentBuilder();
	    doc = db.parse(file);
	    doc.getDocumentElement().normalize();
	    
	} catch (ParserConfigurationException | SAXException | IOException e) {
	    System.out.println("Logujem exception");
	}
    }
    
    
    /**
     * Get value of node
     * @param shopItem
     * @param nodeName
     * @return 
     */
    protected String getValue(Node shopItem, String nodeName)
    {
	Element shopItemEl = (Element) shopItem;
	Node node = shopItemEl.getElementsByTagName(nodeName).item(0);
	String value = node.getChildNodes().item(0).getNodeValue();
	
	return value;
    }
}
